<?php

declare(strict_types=1);

namespace XOne\Bundle\MediaObjectBundle\EventListener;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\PersistentCollection;
use Psr\Log\LoggerInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\PropertyAccess\PropertyAccess;
use XOne\Bundle\MediaObjectBundle\Entity\AbstractMediaObject;
use XOne\Bundle\MediaObjectBundle\Factory\MediaObjectFactory;
use XOne\Bundle\MediaObjectBundle\Form\MediaObjectType;

readonly class MediaObjectTypeUploadListener
{
    public function __construct(
        private EntityManagerInterface $entityManager,
        private RequestStack $requestStack,
        private MediaObjectFactory $mediaObjectFactory,
        private LoggerInterface $logger,
    ) {
    }

    public function submit(FormEvent $event): void {
        $form = $event->getForm();

        if ($deletedMediaObjectIds = $this->getDeletedMediaObjectIds($form)) {
            $this->handleDelete($form, $deletedMediaObjectIds);
        }

        $this->handleUpload($event);
    }

    private function getDeletedMediaObjectIds(FormInterface $form): array
    {
        // The below code is a work-around to retrieve deleted media object IDs from the form
        // As FileType from which we aren't inheriting isn't compound, we are extracting the data straight from the request

        // Build a traversal path for nested forms - form1.form2.form3, ...
        $traversePath = [];
        $currentNode = $form;
        while (!is_null($currentNode)) {
            $traversePath[] = $currentNode->getName();
            $currentNode = $currentNode->getParent();
        }

        // The last inserted element will be the root
        $root = array_pop($traversePath);
        // As we will need to iterate from end to start, let's reverse the array
        $traversePath = array_reverse($traversePath);

        // Traverse the request according to the Form names
        $request = $this->requestStack->getCurrentRequest();
        $requestData = $request->get($root);
        foreach ($traversePath as $arrayKey) {
            $requestData = $requestData[$arrayKey] ?? [];
        }

        // Access deleted media object IDs directly from the request
        return $requestData[MediaObjectType::DELETE_FORM_KEY] ?? [];
    }

    private function handleUpload(FormEvent $event): void
    {
        $form = $event->getForm();

        // If we have an A.attachment entity with AType having MediaObjectType for attachment field
        //   then $formEntity will return A entity value, which allows us to retrieve existing files
        // Note: this can be null in case of compound form types with uninitialized relations
        // Consider relation A.B.C.attachment, with A 1-1 B 1-1 C, and A.b = null
        //   - in this case C will return null, instead of an empty entity class
        $formEntity = $form->getParent()->getData();
        $fieldName = $form->getName();

        $propertyAccessor = PropertyAccess::createPropertyAccessor();
        $currentFiles = null;
        // If the parent form entity exists, then it's possible that some files are already saved
        // Let's try to read them - we might want to delete them later, or append to them
        if (!is_null($formEntity)) {
            $currentFiles = $propertyAccessor->getValue($formEntity, $fieldName);
        }

        // The submitted FileType can actually be empty
        // If it is empty, we should refrain from further processing and eager return
        if (!$event->getData()) {
            // If there are existing files on the entity,
            // we must manually set them, so the stored data doesn't get lost
            if (!is_null($currentFiles)) {
                $event->setData($currentFiles);
            }

            // We don't handle other cases, as if the form entity is NULL,
            //   and the field hasn't been filled during submission,
            //   then the NULL returned by ViewTransformer should be correct
            // TODO: What about default values? Is an overwrite possible? Would the form NULL even impact a default value?
            return;
        }

        // Check if we're uploading multiple files or a single file
        $areMultipleFiles = $form->getConfig()->getOption('multiple');

        if ($areMultipleFiles) {
            $currentFiles ??= new ArrayCollection();

            // Merge existing files with uploaded ones
            foreach ($event->getData() ?? [] as $uploadedFile) {
                $fileEntity = $this->mediaObjectFactory->createMediaObjectForFormField($event);

                // Let's consider a 1-N relation, for ex: A.files, with OneToMany `files` field on `A`
                //
                // If there's a property on the File entity, `a` ManyToOne `A`, it will allow us to access A from file
                // Typically, Symfony will set this automatically:
                // Autogenerated setter in A.addFile() will set the inverse relationship (file.setA)
                //
                // However, in MediaObjectType we're operating on the entire collection
                // We're replacing the whole collection, which in turn won't call setters on individual elements
                // We need to set `a` property in new File manually

                /** @see https://tickets.x-one.pl/issues/40234 */
                if (!is_null($formEntity)) {
                    // Check how's the `files` property named in the parent entity
                    $property = new \ReflectionProperty($formEntity, $form->getName());
                    $attributes = $property->getAttributes(OneToMany::class);
                    foreach ($attributes as $attribute) {
                        $arguments = $attribute->getArguments();
                        if (!empty($arguments['mappedBy'])) {
                            $mappedBy = $arguments['mappedBy'];
                            $propertyAccessor->setValue($fileEntity, $mappedBy, $formEntity);
                        }
                    }
                }

                $fileEntity->setFile($uploadedFile);
                $currentFiles[] = $fileEntity;
            }

            $event->setData($currentFiles);
        } else {
            $fileEntity = $this->mediaObjectFactory->createMediaObjectForFormField($event);
            $fileEntity->setFile($event->getData());

            // Remove existing file, keep the uploaded one
            if ($currentFiles) {
                $this->entityManager->remove($currentFiles);
            }

            $event->setData($fileEntity);
        }
    }

    private function handleDelete(FormInterface $form, array $deletedMediaObjects): void
    {
        $propertyAccessor = PropertyAccess::createPropertyAccessor();
        $formEntity = $form->getParent()->getData();
        $fieldName = $form->getName();

        if (is_null($formEntity)) {
            // This shouldn't happen. $formEntity can be NULL, if there is no pre-existing entity data
            // But if there is no pre-existing data, then there are no files to be deleted,
            //   and therefore this function should not be called!
            // It can however happen by manually crafting a request
            $this->logger->warning('MediaObjectBundle: handleDelete called, but formEntity is null - bug?');
            return;
        }

        $currentFiles = $propertyAccessor->getValue($formEntity, $fieldName);
        $areMultipleFiles = $form->getConfig()->getOption('multiple');

        if (!$areMultipleFiles) {
            $currentFiles = [$currentFiles];
        }

        /** @var AbstractMediaObject[] $currentFiles */
        foreach ($currentFiles as $file) {
            if (in_array((string) $file->getId(), $deletedMediaObjects)) {
                $this->entityManager->remove($file);
                if($file && $form->getData() instanceof Collection && $form->getData()->contains($file)) {
                    $form->getData()->removeElement($file);
                }
            }
        }
    }
}
