<?php

declare(strict_types=1);

namespace XOne\Bundle\MediaObjectBundle\Transformer;

use Mimey\MimeTypes;

class MimeTypeTransformer
{
    public function getMimeTypesForExtensions(array $fileExtensions): array
    {
        $mimes = new MimeTypes();
        $mimeTypes = [];

        foreach ($fileExtensions as $mime) {
            $mimeTypes = array_merge($mimeTypes, $mimes->getAllMimeTypes($mime));
        }

        return array_unique($mimeTypes);
    }
}
