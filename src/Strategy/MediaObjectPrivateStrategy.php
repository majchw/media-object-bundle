<?php

declare(strict_types=1);

namespace XOne\Bundle\MediaObjectBundle\Strategy;

use XOne\Bundle\MediaObjectBundle\Entity\AbstractMediaObject;
use XOne\Bundle\MediaObjectBundle\Provider\MediaObjectVichMappingProvider;

class MediaObjectPrivateStrategy
{
    public function __construct(
        private MediaObjectVichMappingProvider $mappingProvider
    )
    {
    }

    public function isPrivateFile(AbstractMediaObject $mediaObject): bool
    {
        $mapping = $this->mappingProvider->getVichUploaderMapping($mediaObject);
        return $this->isPrivateVichMapping($mapping);
    }

    public function isPrivateVichMapping(array $vichUploaderMapping): bool
    {
        return 'private' === $vichUploaderMapping['uri_prefix'];
    }
}
