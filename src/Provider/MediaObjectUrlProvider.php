<?php

declare(strict_types=1);

namespace XOne\Bundle\MediaObjectBundle\Provider;


use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Vich\UploaderBundle\Templating\Helper\UploaderHelper;
use XOne\Bundle\MediaObjectBundle\Entity\AbstractMediaObject;
use XOne\Bundle\MediaObjectBundle\Strategy\MediaObjectPrivateStrategy;

class MediaObjectUrlProvider
{
    public function __construct(
        private UploaderHelper $helper,
        private UrlGeneratorInterface $router,
        private MediaObjectPrivateStrategy $privateStrategy,
    ) {
    }

    public function getMediaObjectUrlForApiAuth(AbstractMediaObject $file): string
    {
        if ($this->privateStrategy->isPrivateFile($file)) {
            // TODO: change me, unimplemented
            return $this->router->generate('x_one_media_object_view', [
                'alias' => $file::getAlias(),
                'id' => $file->getId(),
            ]);
        } else {
            return $this->helper->asset($file);
        }
    }

    public function getMediaObjectUrlForSessionAuth(AbstractMediaObject $file): string
    {
        if ($this->privateStrategy->isPrivateFile($file)) {
            return $this->router->generate('x_one_media_object_view', [
                'alias' => $file::getAlias(),
                'id' => $file->getId(),
            ]);
        } else {
            return $this->helper->asset($file);
        }
    }
}
