<?php

declare(strict_types=1);

namespace XOne\Bundle\MediaObjectBundle\Provider;


use Doctrine\Persistence\ManagerRegistry;
use Vich\UploaderBundle\Mapping\Annotation\UploadableField;
use XOne\Bundle\MediaObjectBundle\Entity\AbstractMediaObject;

class MediaObjectVichMappingProvider
{
    public function __construct(
        private ManagerRegistry       $registry,
        private array                 $mediaObjectMappings,
    )
    {
    }

    public function getVichUploaderMapping(AbstractMediaObject $file): array
    {
        $metadata = $this->registry->getManager()->getClassMetadata(get_class($file));
        $uploadableField = $metadata->getReflectionClass()->getProperty('file');

        $uploadableAttribute = null;
        foreach ($uploadableField->getAttributes() as $attribute) {
            if (UploadableField::class === $attribute->getName()) {
                $uploadableAttribute = $attribute->newInstance();
                break;
            }
        }

        if (is_null($uploadableAttribute)) {
            throw new \Exception('VichUploaderBundle mapping was not found for provided AbstractMediaObject class - did you correctly set an UploadableField attribute on the file property?');
        }

        /** @var UploadableField $uploadableAttribute */
        $mapping = $this->mediaObjectMappings[$uploadableAttribute->getMapping()];

        if (!array_key_exists('uri_prefix', $mapping)) {
            throw new \Exception('uri_prefix key was not found in VichUploaderBundle mapping set on the AbstractMediaObject.file property!');
        }

        if (!array_key_exists('upload_destination', $mapping)) {
            throw new \Exception('upload_destination key was not found in VichUploaderBundle mapping set on the AbstractMediaObject.file property!');
        }

        return $mapping;
    }
}
