<?php

declare(strict_types=1);

namespace XOne\Bundle\MediaObjectBundle\Provider;


use Vich\UploaderBundle\Templating\Helper\UploaderHelper;
use XOne\Bundle\MediaObjectBundle\Entity\AbstractMediaObject;
use XOne\Bundle\MediaObjectBundle\Strategy\MediaObjectPrivateStrategy;

class MediaObjectPathProvider
{
    public function __construct(
        private UploaderHelper $helper,
        private MediaObjectVichMappingProvider $mappingProvider,
        private MediaObjectPrivateStrategy $privateStrategy,
        private string $kernelProjectDir,
    ) {
    }

    public function getMediaObjectPath(AbstractMediaObject $file): string
    {
        $mapping = $this->mappingProvider->getVichUploaderMapping($file);
        $relativePath = $this->helper->asset($file, 'file');

        if ($this->privateStrategy->isPrivateVichMapping($mapping)) {
            $relativePath = substr($relativePath,  strlen('private'));
            return $mapping['upload_destination'] . $relativePath;
        } else {
            return $this->kernelProjectDir . '/public' . $this->helper->asset($file, 'file');
        }
    }
}
