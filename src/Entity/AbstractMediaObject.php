<?php

declare(strict_types=1);

namespace XOne\Bundle\MediaObjectBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\MappedSuperclass;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Serializer\Annotation\Groups;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

#[MappedSuperclass]
abstract class AbstractMediaObject
{
    abstract public static function getAlias(): string;

    #[ORM\Id, ORM\Column, ORM\GeneratedValue]
    #[Groups(['media_object', 'media_object:read'])]
    protected ?int $id = null;

    #[Vich\UploadableField(mapping: 'media_object_default', fileNameProperty: 'filePath')]
    protected ?File $file = null;

    #[ORM\Column(nullable: true)]
    protected ?string $filePath = null;

    #[ORM\Column(nullable: true)]
    protected ?\DateTimeImmutable $updatedAt = null;

    #[Groups(['media_object', 'media_object:read'])]
    /** Set by MediaObjectNormalizer during serialization. */
    public ?string $contentUrl = null;

    #[Groups(['media_object:write'])]
    /** Processed by API denormalizer */
    public ?string $content = null;

    #[Groups(['media_object:write'])]
    /** Processed by API denormalizer */
    public ?string $filename = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFilePath(): ?string
    {
        return $this->filePath;
    }

    public function setFilePath(?string $filePath): self
    {
        $this->filePath = $filePath;

        return $this;
    }

    public function getFileName(): ?string
    {
        return is_null($this->filePath) ? null : basename($this->filePath);
    }

    public function setFile(?File $file): self
    {
        $this->file = $file;

        if (null !== $file) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTimeImmutable();
        }

        return $this;
    }

    public function getFile(): ?File
    {
        return $this->file;
    }
}
