<?php

declare(strict_types=1);

namespace XOne\Bundle\MediaObjectBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Contracts\Translation\TranslatorInterface;
use Vich\UploaderBundle\Handler\DownloadHandler;
use XOne\Bundle\MediaObjectBundle\Entity\AbstractMediaObject;
use XOne\Bundle\MediaObjectBundle\Security\MediaObjectVoter;

#[Route('media-object', name: 'x_one_media_object_')]
class MediaObjectController extends AbstractController
{
    public function __construct(
        private EntityManagerInterface $entityManager,
        private TranslatorInterface $translator,
    ) {
    }

    #[Route('/{alias}/{id}', name: 'delete', requirements: ['id' => '\d+'], methods: ['DELETE'])]
    public function delete(string $alias, int $id): Response
    {
        // We don't delete the media object with a database call,
        //   but rather we first retrieve the media object, then delete it via Doctrine
        // This way we ensure that appropriate VichUploaderBundle callbacks are executed
        //   and that the object actually exists
        try {
            $mediaObject = $this->getMediaObject($alias, $id, MediaObjectVoter::DELETE);
        } catch (AccessDeniedException $exception) {
            return new JsonResponse(['error' => $exception->getMessage()], 403);
        } catch (\Exception $exception) {
            return new JsonResponse(['error' => $exception->getMessage()], 400);
        } catch (\Error $error) {
            return new JsonResponse(['error' => $error->getMessage()], 500);
        }

        $this->entityManager->remove($mediaObject);
        $this->entityManager->flush();

        return new JsonResponse([
            'error' => null,
        ]);
    }

    #[Route('/{alias}/{id}', name: 'view', requirements: ['id' => '\d+'], methods: ['GET'])]
    public function view(string $alias, int $id, DownloadHandler $downloadHandler): Response
    {
        $mediaObject = $this->getMediaObject($alias, $id, MediaObjectVoter::VIEW);

        return $downloadHandler->downloadObject(
            $mediaObject, 'file', forceDownload: false
        );
    }

    private function getMediaObject(string $alias, int $id, string $voterAttribute): AbstractMediaObject
    {
        $fqcn = $this->getMediaObjectFQCNFromAlias($alias);

        $repository = $this->entityManager->getRepository($fqcn);
        if (is_null($repository)) {
            $message = $this->translator->trans(
                'controller.error.repository',
                ['%fqcn%' => $fqcn],
                'x_one_media_object',
            );
            throw new \Error($message);
        }

        /** @var AbstractMediaObject $mediaObject */
        $mediaObject = $repository->find($id);
        if (is_null($mediaObject)) {
            $message = $this->translator->trans(
                'controller.error.not_found',
                ['%id%' => $id, '%alias%' => $alias],
                'x_one_media_object',
            );
            throw new \Exception($message);
        }

        if (!$this->isGranted($voterAttribute, $mediaObject)) {
            $message = $this->translator->trans(
                'controller.error.unauthorized',
                ['%id%' => $id, '%alias%' => $alias],
                'x_one_media_object',
            );
            throw new AccessDeniedException($message);
        }

        return $mediaObject;
    }

    private function getMediaObjectFQCNFromAlias(string $alias): string
    {
        foreach ($this->entityManager->getMetadataFactory()->getAllMetadata() as $metadata) {
            if (!is_a($metadata->rootEntityName, AbstractMediaObject::class, true)) {
                continue;
            }

            if ($alias === $metadata->rootEntityName::getAlias()) {
                return $metadata->getName();
            }
        }

        $message = $this->translator->trans(
            'controller.error.implementation_not_found',
            ['%alias%' => $alias],
            'x_one_media_object',
        );
        throw new \Exception($message);
    }
}
