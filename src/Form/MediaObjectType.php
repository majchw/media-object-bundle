<?php

declare(strict_types=1);

namespace XOne\Bundle\MediaObjectBundle\Form;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Persistence\Proxy;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use XOne\Bundle\MediaObjectBundle\Entity\AbstractMediaObject;
use XOne\Bundle\MediaObjectBundle\EventListener\MediaObjectTypeUploadListener;
use XOne\Bundle\MediaObjectBundle\Mime\MimeTypeGuesser;
use XOne\Bundle\MediaObjectBundle\Transformer\MimeTypeTransformer;

class MediaObjectType extends AbstractType
{
    public const DELETE_FORM_KEY = 'deletedMediaObjects';

    public function __construct(
        private readonly MimeTypeTransformer $mimeTypeTransformer,
        private readonly MediaObjectTypeUploadListener $uploadListener,
        private readonly MimeTypeGuesser $mimeTypeGuesser,
    ) {
    }

    public function getParent(): string
    {
        return FileType::class;
    }

    public function getBlockPrefix(): string
    {
        return 'x_one_media_object';
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->addEventListener(FormEvents::SUBMIT, [$this->uploadListener, 'submit']);

        // We need to add a ViewTransformer
        // Otherwise the underlying FileType receives an AbstractMediaObject instead of a File type and throws errors
        $builder->addViewTransformer(new CallbackTransformer(
            function ($mediaObject) {
                // We're transforming ?AbstractMediaObject to null
                // Why? So the FileType that MediaObjectType bases on renders correctly
                // We're handling the file list ourselves, we don't want FileType to know that any files already exist
                return null;
            },
            function ($uploadedFile) {
                // We don't need reverse transform (view -> normalized)
                // UploadedFile gets processed later on by PRE_SUBMIT event in FileType
                //   and by SUBMIT event in MediaObjectType, getting converted into an AbstractMediaObject
                return $uploadedFile;
            }
        ));
    }

    public function buildView(FormView $view, FormInterface $form, array $options): void
    {
        $propertyAccessor = PropertyAccess::createPropertyAccessor();
        $formEntity = $form->getParent()->getData();
        $fieldName = $form->getName();

        // $formEntity can be null in for ex. compound forms
        if (is_null($formEntity)) {
            $currentFiles = new ArrayCollection();
        } else {
            $currentFiles = $propertyAccessor->getValue($formEntity, $fieldName);
        }

        // Reading from form data can result in a Proxy instance - we have to load it prior to accessing its data
        if ($currentFiles instanceof Proxy) {
            $currentFiles->__load();
        }

        // Ensure that view always receives an array of files, even when there are no files, or a single file
        if (!($currentFiles instanceof Collection)) {
            if (is_null($currentFiles)) {
                $currentFiles = new ArrayCollection();
            } else {
                $currentFiles = new ArrayCollection([$currentFiles]);
            }
        }

        $file_extensions_with_dots = array_map(static function ($extension) {
            return '.' . $extension;
        }, $options['allowed_file_extensions']);
        $accepted_mime_types = $this->mimeTypeTransformer->getMimeTypesForExtensions($options['allowed_file_extensions']);

        // We must determine which file extensions can be selected in the <form> element
        // A MIME type can correspond to multiple file extensions
        // @see https://github.com/symfony/symfony/blob/7.2/src/Symfony/Component/Mime/MimeTypes.php
        // However, if we validate file extensions, the list will be strictly limited to those
        if ($options['validate_file_extensions']) {
            $accepted_file_types = $file_extensions_with_dots;
        }
        // If we don't validate file extensions, but MIME types,
        //   the browser will infer available extensions via provided MIME type
        //   and the mime types can get passed to the accept attribute
        // Note: this can cause a slight mismatch if our libraries don't fully match browser implementation
        //   and one file extension is marked for a MIME type on browser side, but not in our library
        //   However, I don't think it's preventable
        elseif ($options['validate_mime_types'])
        {
            $accepted_file_types = array_merge($file_extensions_with_dots, $accepted_mime_types);
        }
        // If we don't validate anything, then we can accept all file types
        else
        {
            $accepted_file_types = ['*'];
        }
        $accepted_file_types = join(",", $accepted_file_types);

        // The user can see a list of available extensions if validation is enabled
        $list_of_file_extensions = $options['allowed_file_extensions'];
        if (!$options['validate_file_extensions'] && !$options['validate_mime_types']) {
            // If validation is disabled, the user should not see it
            $list_of_file_extensions = [];
        }


        $view->vars['files'] = $currentFiles;
        $view->vars['allowed_mime_types'] = $accepted_mime_types;
        $view->vars['allowed_file_extensions'] = $list_of_file_extensions;
        $view->vars['accepted_file_types'] = $accepted_file_types;
        $view->vars['delete_form_key'] = self::DELETE_FORM_KEY;
        $view->vars['multiple'] = $options['multiple'];
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'required' => false,
            'multiple' => false,
            'allow_extra_fields' => true,
            'validate_mime_types' => true,
            'validate_file_extensions' => false,
            'allowed_file_extensions' => [],
            'constraints' => [
                new Assert\Callback([$this, 'validateMimeTypesAndExtensions']),
            ],
        ]);

        $resolver->setRequired([
            'allowed_file_extensions',
        ]);
        $resolver->setAllowedTypes('allowed_file_extensions', ['array']);
        $resolver->setNormalizer('allowed_file_extensions', static function (Options $options, $allowedFileExtensions): array {
            if (!$options['validate_file_extensions'] && !$options['validate_mime_types']) {
                // If we don't validate anything, we don't need to check for allowed file extensions
                return [];
            }

            if (empty($allowedFileExtensions)) {
                throw new \Error('allowed_file_extensions must not be empty!');
            }

            return $allowedFileExtensions;
        });
    }

    public function validateMimeTypesAndExtensions($mediaObjects, ExecutionContextInterface $context): void
    {
        $formOptions = $context->getObject()->getConfig()->getOptions() ?? [];
        $allowedMimeTypes = $this->mimeTypeTransformer->getMimeTypesForExtensions($formOptions['allowed_file_extensions']);

        // $files can either be one UploadedFile, an array of UploadedFile, or null
        $mediaObjects ??= [];
        if (!(is_array($mediaObjects) || is_a($mediaObjects, Collection::class, true))) {
            $mediaObjects = [$mediaObjects];
        }

        /** @var AbstractMediaObject[] $mediaObjects */
        foreach ($mediaObjects as $mediaObject) {
            if (is_null($mediaObject->getFile())) {
                // Omit files that have already been uploaded, or don't have File set
                continue;
            }
            $file = $mediaObject->getFile();
            $buildFileTypeViolation = false;

            if ($formOptions['validate_mime_types']) {
                $mimeType = $this->mimeTypeGuesser->guessMimeType($file->getRealPath());
                $fileHasValidMimeType = false;
                foreach ($allowedMimeTypes as $allowedMimeType) {
                    // Check for exact match
                    if ($allowedMimeType === $mimeType) {
                        $fileHasValidMimeType = true;
                        break;
                    }
                }
                $buildFileTypeViolation = $buildFileTypeViolation || !$fileHasValidMimeType;
            }

            if ($formOptions['validate_file_extensions']) {
                $fileExtension = $file->getClientOriginalExtension();
                $fileHasValidExtension = in_array($fileExtension, $formOptions['allowed_file_extensions'], true);
                $buildFileTypeViolation = $buildFileTypeViolation || !$fileHasValidExtension;
            }

            if ($buildFileTypeViolation) {
                $context
                    ->buildViolation('msg.invalid_submitted_filetype')
                    ->setTranslationDomain('x_one_media_object')
                    ->addViolation();
            }
        }
    }
}
