<?php

declare(strict_types=1);

namespace XOne\Bundle\MediaObjectBundle\Security;

use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use XOne\Bundle\MediaObjectBundle\Entity\AbstractMediaObject;

class MediaObjectVoter extends Voter implements MediaObjectVoterInterface
{
    public const DELETE = 'delete-media-object';
    public const VIEW = 'view-media-object';

    public function getSupportedClassFQCN(): string
    {
        return AbstractMediaObject::class;
    }

    public function canDelete(AbstractMediaObject $subject, TokenInterface $token): bool
    {
        // By default, you cannot delete an AbstractMediaObject, unless you explicitly create a voter
        return false;
    }

    public function canView(AbstractMediaObject $subject, TokenInterface $token): bool
    {
        // By default, you cannot view an AbstractMediaObject, unless you explicitly create a voter
        return false;
    }

    protected function supports(string $attribute, mixed $subject): bool
    {
        if (!in_array($attribute, [self::DELETE, self::VIEW])) {
            return false;
        }

        if (!is_a($subject, $this->getSupportedClassFQCN(), true)) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute($attribute, mixed $subject, TokenInterface $token): bool
    {
        return match ($attribute) {
            self::DELETE => $this->canDelete($subject, $token),
            self::VIEW => $this->canView($subject, $token),
            default => false,
        };
    }
}
