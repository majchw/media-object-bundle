<?php

declare(strict_types=1);

namespace XOne\Bundle\MediaObjectBundle\Security;

use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use XOne\Bundle\MediaObjectBundle\Entity\AbstractMediaObject;

interface MediaObjectVoterInterface
{
    /** Returns FQCN of an entity that extends AbstractMediaObject. */
    public function getSupportedClassFQCN(): string;

    /** Returns true if the provided token can delete the provided media object.
     *
     * Used in the dedicated controller, which is used for asynchronous deletion, outside Symfony forms.
     *
     * Files can be still deleted via Symfony forms, even if this function returns false.
     */
    public function canDelete(AbstractMediaObject $subject, TokenInterface $token): bool;

    /** Returns true if the provided token can view the provided media object.
     *
     * Used in the dedicated controller, which is used for displaying private files, outside the website path.
     * Consider disabling this access method, if you're not explicitly using it.
     *
     * Public files can still be viewed via the directory path, even if this function returns false.
     */
    public function canView(AbstractMediaObject $subject, TokenInterface $token): bool;
}
