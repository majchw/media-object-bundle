<?php

declare(strict_types=1);

namespace XOne\Bundle\MediaObjectBundle\Twig;

use Doctrine\Common\Collections\Collection;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Twig\Environment;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;
use Vich\UploaderBundle\Mapping\Annotation\UploadableField;
use Vich\UploaderBundle\Templating\Helper\UploaderHelper;
use XOne\Bundle\MediaObjectBundle\Entity\AbstractMediaObject;
use XOne\Bundle\MediaObjectBundle\Provider\MediaObjectUrlProvider;

class MediaObjectExtension extends AbstractExtension
{
    public function __construct(
        private MediaObjectUrlProvider $mediaObjectUrlProvider,
    ) {
    }

    public function getFunctions(): array
    {
        $definitions = [
            'media_objects' => $this->renderMediaObjectsList(...),
            'media_object_asset' => $this->renderMediaObjectAsset(...),
        ];

        $functions = [];

        foreach ($definitions as $name => $callable) {
            $functions[] = new TwigFunction($name, $callable, [
                'needs_environment' => true,
                'is_safe' => ['html'],
            ]);
        }

        return $functions;
    }

    public function renderMediaObjectsList(Environment $environment, mixed $files, $options = []): string
    {
        if (
            !(is_array($files) || $files instanceof Collection) && $files
        ) {
            $files = [$files];
        }

        return $environment->load('@XOneMediaObject/media_object_form.html.twig')->renderBlock('x_one_media_object_list', [
            'files' => $files,
            'display_download' => $options['download'] ?? true,
            'display_async_delete' => $options['delete'] ?? false,
        ]);
    }

    public function renderMediaObjectAsset(Environment $environment, AbstractMediaObject $file): string
    {
        return $this->mediaObjectUrlProvider->getMediaObjectUrlForSessionAuth($file);
    }
}
