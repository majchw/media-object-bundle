<?php

declare(strict_types=1);

namespace XOne\Bundle\MediaObjectBundle\Factory;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\OneToMany;
use Symfony\Component\Form\FormEvent;
use XOne\Bundle\MediaObjectBundle\Entity\AbstractMediaObject;

class MediaObjectFactory
{
    /** Creates a concrete AbstractMediaObject instance for the given form field.
     *
     * For 1-1 relationships, it uses the type hint to determine the class,
     * for N-N relationships, it uses the ManyToMany annotation to determine the class.
     *
     */
    public function createMediaObjectForFormField(FormEvent $event): AbstractMediaObject
    {
        $form = $event->getForm();
        $formEntity = $form->getParent()->getConfig()->getDataClass();
        // TODO: use property path instead
        $fieldName = $form->getName();

        $property = new \ReflectionProperty($formEntity, $fieldName);
        $propertyClass = $property->getType()->getName();
        // The property will either be an AbstractMediaObject for 1-1 relationships
        // Or it will be a Collection, for 1-N relationships
        // In other cases the form type is misconfigured
        if (is_a($propertyClass, AbstractMediaObject::class, true)) {
            return new $propertyClass();
        } elseif (is_a($propertyClass, Collection::class, true)) {
            $attributes = $property->getAttributes(ManyToMany::class);
            $attributes = !empty($attributes) ? $attributes : $property->getAttributes(OneToMany::class);
            $fqcn = null;
            foreach ($attributes as $attribute) {
                $fqcn ??= $attribute->getArguments()['targetEntity'] ?? null;
            }

            if (is_null($fqcn)) {
                throw new \Error("Misconfigured entity - the field's Collection type should have a ManyToMany attribute with targetEntity set.");
            }

            return new $fqcn();
        } else {
            throw new \Error("Misconfigured entity -
            the field's type should either be set to an entity extending AbstractMediaObject (for 1-1 relationships)
             or to Collection (for 1-N relationships) with a ManyToMany attribute.");
        }
    }
}
