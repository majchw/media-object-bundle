<?php

declare(strict_types=1);

namespace XOne\Bundle\MediaObjectBundle\Mime;

use Symfony\Component\Mime\MimeTypes;

class MimeTypeGuesser
{
    protected MimeTypes $mimeTypes;

    public function __construct() {
        $this->mimeTypes = new MimeTypes();
        $this->mimeTypes->registerGuesser(new FileBinaryMimeTypeGuesserProxy());
    }

    public function guessMimeType(string $filePath): string|null
    {
        return $this->mimeTypes->guessMimeType($filePath);
    }
}
