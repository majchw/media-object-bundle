<?php

declare(strict_types=1);

namespace XOne\Bundle\MediaObjectBundle\Serializer\Normalizer;

use Symfony\Component\Serializer\Normalizer\NormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use XOne\Bundle\MediaObjectBundle\Entity\AbstractMediaObject;
use XOne\Bundle\MediaObjectBundle\Provider\MediaObjectUrlProvider;

class MediaObjectNormalizer implements NormalizerInterface, NormalizerAwareInterface
{
    use NormalizerAwareTrait;

    private const ALREADY_CALLED = 'MEDIA_OBJECT_NORMALIZER_ALREADY_CALLED';

    public function __construct(private readonly MediaObjectUrlProvider $mediaObjectUrlProvider)
    {
    }

    public function normalize($object, string $format = null, array $context = []): array|string|int|float|bool|\ArrayObject|null
    {
        $context[self::ALREADY_CALLED] = true;
        /* @var AbstractMediaObject $object */
        $object->contentUrl = $this->mediaObjectUrlProvider->getMediaObjectUrlForApiAuth($object);

        return $this->normalizer->normalize($object, $format, $context);
    }

    public function supportsNormalization($data, string $format = null, array $context = []): bool
    {
        if (isset($context[self::ALREADY_CALLED])) {
            return false;
        }

        return $data instanceof AbstractMediaObject;
    }

    public function getSupportedTypes(?string $format): array
    {
        // Don't cache supportsNormalization for AbstractMediaObject,
        //   as it relies on `$context[self::ALREADY_CALLED]`
        // Otherwise the normalizer goes into an infinite loop,
        //   repeatedly calling itself via recursion
        // If you wish to change this behaviour, fully serialize the object within normalize() call
        return [
            AbstractMediaObject::class => false,
        ];
    }
}
