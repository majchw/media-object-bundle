# Media Object Bundle

Paczka stanowi nakładkę na [VichUploaderBundle](https://github.com/dustin10/VichUploaderBundle) ułatwiającą pracę z uploadowaniem plików.

## Funkcjonalność

- jednoczesne uploadowanie wielu plików
- ograniczenie do dopuszczalnych rozszerzeń
- gotowy formularz, działający out-of-the-box zarówno dla relacji 1-1 jak i 1-N
- usuwanie z poziomu formularza oraz z poziomu listy plików
- obsługa prywatnych plików, dostępnych wyłącznie przez kontroler
- szybka konfiguracja bezpieczeństwa za pośrednictwem Voterów

## Odnośniki

- [Jak zainstalować paczkę?](docs/installation.pl.md)
- [Praktyczny poradnik wykorzystania paczki](docs/usage.pl.md)
- [Reference dla poszczególnych klas](docs/reference.pl.md)

## Znane problemy

- `property_path` prawdopodobnie nie jest odczytywane w `MediaObjectType` (nieprzetestowane)
- pliki w kontrolerze pobierają się pod Edge / Chrome pomimo `Content-Disposition: inline; filename=xyz` - https://stackoverflow.com/questions/71679544/content-disposition-inline-filename-not-working
- formularz odczytuje niektóre pola bezpośrednio z Requestu
- po nieudanym uploadzie nie widać listy aktualnych plików
- trzeba ręcznie zaktualizować plik `routes.yaml`, bo nie mamy obecnie możliwości stworzenia Recipes
- wykorzystywany UX Translator wg autorów [nie jest wspierany stricte dla bundle](https://github.com/symfony/ux/issues/1034)

## Feature do zaimplementowania

- pobieranie wielu plików jednocześnie
- pełnoprawna integracja z API Platform, potencjalnie w drugiej paczce, np. media-object-api-bundle? - nie każdy projekt będzie miał API
