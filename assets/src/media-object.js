import { Controller } from '@hotwired/stimulus';
import {
    trans,
    FILE_DELETE_CONFIRM,
    FILE_DELETE_UNKNOWN_ERROR,
    FILE_DELETE_ERROR
} from './../translator';

export default class extends Controller {
    static targets = ["deleteAsyncFile"]

    connect() {
        this.deleteAsyncFileTargets.forEach(deleteAction => {
            deleteAction.addEventListener("click", (e) => {
                e.preventDefault();
                this.onAsyncDelete(e);
            });
        });

    }

    async onAsyncDelete(e) {
        const confirmation = confirm(trans(FILE_DELETE_CONFIRM, {}, 'x_one_media_object'));
        if (!confirmation) {
            return console.debug("User didn't confirm the asynchronous deletion.");
        }

        const link = e.currentTarget;
        const deleteEndpoint = link.href;

        try {
            const response = await fetch(deleteEndpoint, {
                method: "DELETE",
            });

            if (response.ok) {
                console.debug("The file was deleted.");
                link.parentElement.remove();
            } else {
                const data = await response.json();
                const errorMessage = data.error || trans(FILE_DELETE_UNKNOWN_ERROR, {}, 'x_one_media_object');
                alert(errorMessage);
            }
        } catch (error) {
            alert(trans(FILE_DELETE_ERROR, {}, 'x_one_media_object'));
        }
    }
}

