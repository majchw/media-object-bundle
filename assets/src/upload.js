import { Controller } from '@hotwired/stimulus';

export default class extends Controller {
    static targets = ["deleteFile"]
    static values = {
        disabled: Boolean,
        multiple: Boolean,
        cssdisabled: String
    };

    connect() {
        this.deleteFileTargets.forEach(deleteAction => {
            deleteAction.addEventListener("click", this.onFormDelete);
        });
    }

    onFormDelete(e) {
        e.preventDefault();

        const link = e.currentTarget;
        const deleteName = link.dataset.deleteName;
        const deleteValue = link.dataset.deleteValue;

        if (!(deleteName && deleteValue)) {
            console.error("User tried to delete a media object, but deleteName & deleteLink aren't set in the HTML! Was form_name set correctly?");
            return;
        }

        const fileContainer = link.parentElement;
        const formContainer = fileContainer.parentElement;

        fileContainer.remove();

        // We signal that a file was deleted by creating an input with its ID
        const deletedFileInput = document.createElement("input");
        deletedFileInput.value = deleteValue;
        deletedFileInput.name = deleteName;
        deletedFileInput.type = "hidden";

        formContainer.appendChild(deletedFileInput);

        console.debug("Marking a media object for deletion", deletedFileInput);

        // Enable the input, if a single file in a non-multiple, enabled input was deleted
        if (!this.multipleValue && !this.disabledValue) {
            const classes = this.cssdisabledValue ?? 'pe-none disabled';
            if (this.cssdisabledValue == null) {
                console.debug('Unable to read cssdisabled value!');
            }

            classes.split(" ").forEach(cssClass => {
                formContainer.querySelector("input[type='file']").classList.remove(cssClass);
            });
        }
    }
}

