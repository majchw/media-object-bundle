# Jak działa uploadowanie plików?
Mechanizm uploadowania plików wykorzystuje VichUploaderBundle.
VichUploaderBundle operuje na koncepcie tzw. mappingów - każdy identyfikator, zwany mappingiem, posiada odrębną konfigurację dla uploadowanych plików.
Mappingami możemy zarzadzać w pliku `vich_uploader.yaml`:

```yml
vich_uploader:
  db_driver: orm
  mappings:
    media_object_default:
      uri_prefix: '/uploads/media_object'
      upload_destination: '%kernel.project_dir%/public/uploads/media_object'
      namer: Vich\UploaderBundle\Naming\SmartUniqueNamer
      directory_namer:
        service: Vich\UploaderBundle\Naming\SubdirDirectoryNamer
        options: { chars_per_dir: 1, dirs: 2 } # will create directory "a/b" for "abcdef.jpg"
```

W powyższej przykładowej konfiguracji dostępny jest jeden mapping **media_object_default** z następującymi parametrami:

- **upload_destination** określa folder, do którego trafią uploadowane pliki;
- **uri_prefix** służy do generowania linków do zasobów, widocznych w np. API;
- **namer** wskazuje nam na Namera, czyli klasę określającą w jaki sposób nazwać uploadowany zasób - w tym przypadku *SmartUniqueNamer* dodaje postfix do nazw plików, co gwarantuje, że nie wystąpią kolizje nazw;
- **directory_namer** określa sposób generowania podkatalogów. Parametr możemy pominąć, wtedy wszystkie pliki trafią bezpośrednio do katalogu wskazanego w upload_destination.

# Uploadowanie plików na przykładzie
Przyjmijmy, że mamy encję `DummyInvoice`, do której chcemy załączać:

- 1 plik PDF, który powinien trafić do folderu `/uploads/invoice`;
- N plików ze skanami, które powinny trafić do folderu `/uploads/invoice_scan`, a następnie do dowolnych podkatalogów.


## Konfiguracja vich_uploader.yaml

Zacznijmy od utworzenia 2 mappingów - nazwijmy je `invoice` oraz `invoice_scan`:
```yml
vich_uploader:
  # [...]
  mappings:
    # [...]
    invoice:
      uri_prefix: '/uploads/invoice'
      upload_destination: '%kernel.project_dir%/public/uploads/invoice'
      namer: Vich\UploaderBundle\Naming\SmartUniqueNamer
    invoice_scan:
      uri_prefix: '/uploads/invoice_scan'
      upload_destination: '%kernel.project_dir%/public/uploads/invoice_scan'
      namer: Vich\UploaderBundle\Naming\SmartUniqueNamer
      directory_namer:
        service: Vich\UploaderBundle\Naming\SubdirDirectoryNamer
        options: { chars_per_dir: 1, dirs: 2 }
```

Pliki w mappingu `invoice` będą trafiały bezpośrednio do katalogu `/uploads/invoice`, zaś pliki w mappingu `invoice_scan` będą trafiały do podkatalogów `/uploads/invoice_scan/a/b`, gdzie a i b to dwie pierwsze litery plików.

Istotna uwaga - parametr `directory_namer` powinniśmy skonfigurować przed rozpoczęciem uploadowania plików. Jeśli skonfigurujemy go po wrzuceniu plików na serwer, starsze pliki, niezgodne z nowym schematem, będą generować błędne URL w obrębie aplikacji.

## Stworzenie encji z plikami

Po skonfigurowaniu `vich_uploader.yml` będziemy musieli utworzyć 2 odrębne encje, ponieważ pole w encji wykorzystuje dokładnie jeden mapping. Nazwijmy je `DummyInvoiceScan` oraz `DummyInvoicePdf`:

```php
<?php

declare(strict_types=1);

namespace App\Entity\Dummy\Invoice;

use App\Core\Entity\AbstractMediaObject;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

#[Vich\Uploadable]
#[ORM\Entity]
class DummyInvoicePdf extends AbstractMediaObject
{
    #[Vich\UploadableField(mapping: 'invoice', fileNameProperty: 'filePath')]
    protected ?File $file = null;

    public static function getAlias(): string
    {
        return 'invoice-pdf';
    }
}
```
```php
<?php

declare(strict_types=1);

namespace App\Entity\Dummy\Invoice;

use App\Core\Entity\AbstractMediaObject;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

#[Vich\Uploadable]
#[ORM\Entity]
class DummyInvoiceScan extends AbstractMediaObject
{
    #[Vich\UploadableField(mapping: 'invoice_scan', fileNameProperty: 'filePath')]
    protected ?File $file = null;

    public static function getAlias(): string
    {
        return 'invoice-scan';
    }
}
```

### Dlaczego nie przechowujemy plików w jednej encji?
Gdybyśmy utworzyli wyłącznie jedną encję, o nazwie np. `EncjaPlik`, wszystkie pliki musiałyby trafiać do jednego katalogu na dysku, zgodnie z ustalonym mappingiem na polu.
Zarządzanie dostępami również stałoby się odrobinę cięższe - operując na jednej klasie `EncjaPlik` nie znamy kontekstu, do którego jest przypisany plik.
Ponadto, gdybyśmy chcieli dodać dodatkowe pole tylko w jednym kontekście, jego wartość dla wszystkich pozostałych plików musiałaby przyjąć wartość `NULL`, gdybyśmy chcieli je utworzyć bezpośrednio na encji.

Z powyższych powodów powstała klasa pomocnicza `AbstractMediaObject`, która ułatwia stworzenie encji z plików.

### Co robi getAlias()?

Metoda `getAlias()` powinna zwracać unikalny identyfikator w obrębie aplikacji. Nazwa jest dowolna, przy czym zalecamy kebab-case.
Wartość tej funkcji jest wykorzystywana do generowania URL do usuwania zdjęć.

### Dlaczego nadpisujemy $file?

Nadpisujemy `$file`, aby móc zaktualizować atrybut `#[Vich\UploadableField]` - zauważcie, że zmieniamy w nim pole mapping.
Atrybuty możemy również nakładać, aby np. aktualizować grupy serializacji, wykorzystywane powszechnie w API Platform.

### Czy musimy nadpisać $file i wykorzystać nowy mapping?

Nie, ale jest to zalecane - oddzielny mapping ułatwi zarządzanie plikami na dysku.

## Powiązanie plików z istniejącą encją

Po utworzeniu `DummyInvoicePdf` oraz `DummyInvoiceScan` możemy dodać relacje do naszej encji `DummyInvoice`:
```php
#[ORM\ManyToMany(targetEntity: DummyInvoiceScan::class, cascade: ['persist', 'remove'])]
private Collection $pdfPages;

#[ORM\OneToOne(cascade: ['persist', 'remove'])]
#[ORM\JoinColumn(onDelete: 'SET NULL')]
private ?DummyInvoicePdf $onePdf = null;
```

Posługujemy się odpowiednio relacją `ManyToMany` oraz `OneToOne`. Zmiana po strona encji nie jest w żadnym stopniu wymagana.

### Dlaczego odwołujemy się do encji w relacji 1-1, zamiast wykorzystać bezpośrednio VichUploaderBundle?

Odwoływanie się wszędzie do encji opartych o `AbstractMediaObject` ułatwia nam zarządzanie plikami oraz wprowadza spójność - okazuje się to szczególnie przydatne w momencie projektowania API, gdy chcemy, aby każde pole z plikami miało możliwie taki sam format.

### Czy onDelete: SET NULL jest wymagane?
`#[ORM\JoinColumn(onDelete: 'SET NULL')]` jest wymagane - inaczej nie będziemy mogli usuwać plików w relacji 1-1.

## Utworzenie formularza
Po zaktualizowaniu encji możemy utworzyć formularz:

```php
class DummyInvoiceFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add('pdfPages', MediaObjectType::class, [
            'label' => 'Pliki umowy',
            'multiple' => true,
            'allowed_file_extensions' => ['jpg', 'png', 'webp', 'pdf'],
        ]);

        $builder->add('onePdf', MediaObjectType::class, [
            'label' => 'Umowa',
            'multiple' => false,
            'allowed_file_extensions' => ['pdf'],
        ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => DummyInvoice::class,
        ]);
    }
}
```

W obu polach wskazujemy na pomocniczy typ `MediaObjectType`, który umożliwia nam dodawanie i usuwanie plików:

![formularz.png](assets/form.png)

### Jak mogę umożliwić dodawanie wszystkich rodzajów plików?

Należy wyłączyć walidację rozszerzeń plików oraz typów MIME:

```php
$builder->add('file', MediaObjectType::class, [
    'validate_mime_types' => false,
    'validate_file_extensions' => false,
]);
```

### Dodałem rozszerzenie pliku, ale nie mogę go dodać przez przeglądarkę. Co jest nie tak?

Prawdopodobnie problem leży w wewnętrznym mappingu pomiędzy typami MIME a rozszerzeniami plików.

Możesz wyłączyć walidację typów MIME i włączyć walidację rozszerzeń plików:

```php
$builder->add('file', MediaObjectType::class, [
    'validate_mime_types' => false,
    'validate_file_extensions' => true,
    'allowed_file_extensions' => ['md'],
]);
```

Wtedy pliki będą walidowane wyłącznie po rozszerzeniu podanym przez uzytkownika.

## Kontroler

W kontrolerze nie musimy dopisywać specjalnego kodu, aby upload plików działał:

```php
#[Route('/{id}/update', name: 'update', methods: ['GET|POST'])]
    public function update(DummyInvoice $invoice, Request $request, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(DummyInvoiceFormType::class, $invoice);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('panel_dummy_invoice_index');
        }

        return $this->render('dummy/invoice/update.html.twig', [
            'form' => $form,
            'invoice' => $invoice,
        ]);
    }
```

### Po wywołaniu persist() usuwanie plików przestało działać
Zachowanie jest w pełni zgodne z Doctrine - w obrębie formularza pliki zostaną oznaczone do usunięcia. Wywołanie persist() na głównej encji spowoduje kaskadowe wywołanie PERSIST na usuniętych plikach, przez co nie zostaną usunięte.


## Template w Twigu

Templatka nie wymaga żadnej ingerencji. Wystarczy nam najprostsze:

```twig
{{ form(form) }}
```

Niezależnie jak wyrenderujemy formularz, nie musimy wyświetlać pola do uploadowania plików w żaden specjalny sposób:

```twig
<div class="card">
    {{ form_start(form) }}
    <div class="card-body">
        {{ form_rest(form) }}
    </div>
    <div class="card-footer text-end">
        <button class="btn btn-primary">{{ 'Zapisz'|trans }}</button>
    </div>
    {{ form_end(form) }}
</div>
```

## Wyświetlenie listy z plikami

Do wyświetlenia listy z plikami możemy posłużyć się funkcją `media_objects(files)`:

```twig
{{ media_objects(invoice.pdfPages) }} {# kolekcja plików #}
{{ media_objects(invoice.onePdf) }} {# 1 plik #}
```

Funkcja przyjmuje następujące parametry:

```twig
{{ media_objects(invoice.pdfPages, {
    delete: true,
    download: false
}) }}
```

- **delete** - czy wyświetlać przycisk Usuń;
- **download** - czy wyświetlać przycisk Pobierz.

Usuwanie z poziomu listy wykorzystuje mechanizm usuwania asynchronicznego.
Jeśli wyświetlimy listę w obrębie formularza oraz naciśniemy usuń, to plik się usunie od razu.
Usuwanie plików z poziomu formularza powinno być realizowane z wykorzystaniem `MediaObjectType` w celu zapewnienia spójności.

## Usuwanie asynchroniczne

Usuwanie asynchroniczne nie wymaga żadnej dodatkowej konfiguracji (poza dostępem w postaci Voterów).
Pod spodem zapytania są wysyłane w formacie `DELETE /media-objects/alias/id`, gdzie alias stanowi wartość `AbstractMediaObject.getAlias()`.
Odpowiedź błędu z kontrolera w formacie `{error: 'wiadomość'}` może zostać wyświetlona użytkownikowi.

## Konfiguracja Voterów

Domyślnie pliki nie mogą być usuwane z wykorzystaniem endpointu do usuwania asynchronicznego.
Aby móc usuwać pliki, musimy skonfigurować dostęp z wykorzystaniem Voterów.

Przyjmijmy, że chcemy umożliwić wszystkim użytkownikom panelu administracyjnego usuwanie PDFów w `DummyInvoice`:

```php
<?php

declare(strict_types=1);

namespace App\Security\Dummy;

use App\Core\Entity\AbstractMediaObject;
use App\Core\Security\MediaObjectVoter;
use App\Core\Security\MediaObjectVoterInterface;
use App\Entity\Dummy\Invoice\DummyInvoicePdf;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

class DummyInvoicePdfVoter extends MediaObjectVoter
{
    public function getSupportedClassFQCN(): string
    {
        return DummyInvoicePdf::class;
    }

    public function canDelete(AbstractMediaObject $subject, TokenInterface $token): bool
    {
        /* @var DummyInvoicePdf $subject */
        return true;
    }
}
```

Klasa `MediaObjectVoter` ułatwia nam pisanie voterów. W `getSupportedClassFQCN` zwracamy FQCN encji dziedziczącej po `AbstractMediaObject`,
zaś w canDelete ustalamy, czy dany token może usunąć dany plik. Z zmiennej `$token` możemy wyciągnąć m.in. aktualnego użytkownika.

## Prywatne pliki

Niektóre pliki powinny być niedostępne z poziomu niezabezpieczonego URL (jak chociażby faktury, czy inne dokumenty poufne). W tym celu wprowadzamy koncept plików prywatnych, które trafiają do katalogu `/resources/uploads`, niedostępnego z poziomu przeglądarki.

Przyjmijmy, że nasz plik z fakturą (DummyInvoicePdf) powinien trafić do katalogu prywatnego. W pierwszej kolejności zmieńmy konfigurację w `vich_uploader.yaml`:

```yaml
invoice:
  uri_prefix: 'private'
  upload_destination: '%kernel.project_dir%/resources/uploads/invoice'
```

Wartość `uri_prefix` została ustawiona na specjalną wartość **private**, która oznacza, że plik jest niedostępny z poziomu przeglądarki. W `upload_destination` wskazujemy zaś na katalog `/resources/uploads/invoice`.

Po zuploadowaniu faktury możemy zauważyć, że wygenerowane linki kierują na `/media-object/invoice-pdf/9`. Bezpośrednio po wejściu otrzymamy błąd: "Nie masz uprawnień do pliku o ID 9 w obrębie aliasu invoice-pdf!". Pozostaje nam skonfigurować naszego Votera - `DummyInvoicePdfVoter` - poprzez dodanie funkcji `canView`:

```php
public function canView(AbstractMediaObject $subject, TokenInterface $token): bool
{
    /* @var DummyInvoicePdf $subject */
    return true;
}
```

Gotowe! Możemy już wyświetlać plik za pomocą kontrolera.

Mechanizm generowania URL poprzez `uri_prefix: 'private'`, oraz otwierania plików za pomocą `/media-object/alias/id` można wykorzystać również dla plików publicznych, dostępnych jako zwykły plik z poziomu przeglądarki - w przypadku, jeśli nie chcemy np. ujawniać ścieżki na dysku użytkownikowi. Należy jednak wziąć pod uwagę, że wiąże się to ze zmniejszoną wydajnością. Standardowy plik jest serwowany za pośrednictwem nginx, bez konieczności przejścia przez php-fpm oraz przetwarzania skryptu.

### vich_uploader_asset() generuje błędne linki dla prywatnych plików

Zamiast `vich_uploader_asset`, wykorzystaj funkcję `media_object_asset(AbstractMediaObject)`. Funkcjonalność `uri_prefix: private` została zaimplementowana w formie rozszerzenia, przez co funkcja wbudowana w VichUploaderBundle nie będzie zwracała poprawnych wyników.

## Jak pobrać odnośnik / ścieżkę do pliku?

- W Twigu możesz wykorzystać `media_object_asset(AbstractMediaObject)` do ustalenia relative URL
- W kodzie możesz wstrzyknąć klasę `MediaObjectUrlProvider` do ustalenia relative URL
- W kodzie możesz wstrzyknąć klasę `MediaObjectPathProvider` do ustalenia ścieżki na dysku dla pliku


## Pobieranie wielu plików jednocześnie

Obecnie mechanizm jest niezaimplementowany.

## API Platform

Obecnie integracja jest niezaimplementowana. Dokumentację związaną z integracją VichUploaderBundle oraz API Platform można znaleźć w https://api-platform.com/docs/core/file-upload/.

# Problematyka

## Klonowanie

Przyjmijmy, że mamy encję odpowiedzialną za pliki, np. `MediaObject`.
MediaObject zawiera rekord o ID 1, wskazujący na plik "obrazek.jpg".
Po sklonowaniu rekordu otrzymamy rekord o ID 2, również wskazujący na plik "obrazek.jpg".

Usunięcie dowolnego z dwóch rekordów spowoduje usunięcie pliku na dysku - może mieć to katastrofalne skutki.

Poniżej zawarty jest snippet, który dla biblioteki `myclabs/DeepCopy` duplikuje plik na dysku w momencie klonowania,
generując losowy prefix zgodny z domyślnymi ustawieniami namera `SmartUniqueNamer`.
Snippet będzie wymagał modyfikacji w celu odwołania się do właściwego mappingu, zależnego od encji.
Mappingi są wstrzyknięte przez konstruktor do zmiennej `$mediaObjectUploadPath`.

```php
$copier->addTypeFilter(new TypeReplaceFilter(function (MediaObject $mediaObject) {
    $newMediaObject = new MediaObject();
    $newMediaObject->source = $mediaObject->source;

    // Generate a new random prefix
    $randomPrefix = bin2hex(random_bytes(6));

    // Replace the prefix in the filename
    $slugger = new AsciiSlugger();
    $newMediaObject->filePath = $slugger->slug($randomPrefix).substr($mediaObject->filePath, 13);

    $fileDirectory = $this->mediaObjectUploadPath['media_object']['upload_destination'].'/';

    $originalFilePath = $fileDirectory.$mediaObject->filePath;
    $newFilePath = $fileDirectory.$newMediaObject->filePath;
    copy($originalFilePath, $newFilePath);

    return $newMediaObject;
}), new TypeMatcher(MediaObject::class));

```

W `services.yaml`:
```yaml
services:
    _defaults:
        bind:
            $mediaObjectUploadPath: '%vich_uploader.mappings%'
```

### Zarządzanie relacjami

Alternatywnym podejściem do klonowania pliku na dysku jest ręczne zarządzanie relacjami, nieprzetarte we wcześniejszych projektach:

> You would need to configure VichUploader to not delete the file and set delete_on_remove: false and delete_on_update: false. That way only the database reference is removed, leaving the file. Then a cron could clean up missing references.

Zamieszczam link do konfiguracji VichUploaderBundle: https://github.com/dustin10/VichUploaderBundle/blob/master/docs/configuration_reference.md


