# Instalacja paczki

Dodaj prywatne repozytorium do pliku `composer.json`:

```json
{
    "repositories": [
        {
            "type": "vcs",
            "url": "git@bitbucket.org:majchw/media-object-bundle.git"
        }
    ]
}
```

Następnie, zainstaluj paczkę:

```shell
composer require x-one/media-object-bundle
```

Symfony Flex powinien automatycznie dodać nowe wpisy do plików `config/bundles.php` oraz `assets/controllers.json`.

**Ręcznie zaktualizuj plik routes.yaml:**

```
x_one_media_object_bundle:
  resource: "@XOneMediaObjectBundle/config/routes.yaml"
```

Finalnie, przebuduj assety:

```shell
yarn install --force
yarn watch
```

## Development
Rozwijając paczkę możemy posłużyć się konfiguracją wskazującą na pliki lokalne:
```
"repositories": [
    {"type": "path", "url": "lib/x-one/media-object-bundle"}
],
```

Oraz:
```
composer require x-one/media-object-bundle dev-master
```
